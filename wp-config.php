<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'whisq');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Lyiqd{7wPGFZc{h7H}1J8)g*N{rzW!>Z6}?PFICRcR_vyh6<J?#TfeC!!o1W^w8c');
define('SECURE_AUTH_KEY',  'PeBzI_(KlRm@yE&},y2C^vtV[;~9+Fvy!aY5XZW;XNAG^$;sC@>Zd=Ca4R166+^-');
define('LOGGED_IN_KEY',    'v<ac79o7Z0XqEzrd1=pRMPg]`MZMg]zz|vhVEzuIl?nkbQo+-J}c5V=C<):E-d]9');
define('NONCE_KEY',        'J5-Kt8Ai`PL!V1@ES%NJH>U5?TRt0lY>s/XS][D=RQ|Pv@:p&ZX`hnqYc.yX~CX#');
define('AUTH_SALT',        'AV[yQ#eK-?!6,%04luIu`U5pv4N{VXs0=KSO;8+0#lK$jur854y=|Y+i9<b7Y[AN');
define('SECURE_AUTH_SALT', 'ny6=^5c$!N|F({M6IotWi_T;G7|0DOl5T#({#CuV4H|cO5TZkVL[L%_Op|h}!t8G');
define('LOGGED_IN_SALT',   'pG;o(aec(dF,P5f91ma5XhA7XR2_WRtPIL?{~|YIxbBbd!dq+#M:N38h,g}Ls<K{');
define('NONCE_SALT',       'E8|W9^RjBMdr7,wZ~gg~=Q69=/#orymx]r7tow!LjQaY%An/BJN80qlQ5ee2</t^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wq_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
