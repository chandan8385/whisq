<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package whisq
 */

?>

	
<?php wp_footer(); ?>
<section id="footer">
      <div class="container">
          <div class="row footer">
              <div class="col-md-12 copyright">
                <?php if ( is_active_sidebar( 'whisq_footer_copyright' ) ) : ?>
                  <div id="whisq_footer" class="kht-widget-area">
                   <?php dynamic_sidebar( 'whisq_footer_copyright' ); ?>
                  </div>
                <?php endif; ?>
              </div>
          </div>
      </div>      
    </section>

    <!-- Page Content -->

    <!-- Bootstrap core JavaScript -->
    <!--script src="<?php bloginfo('template_directory'); ?>/assets/vendor/jquery/jquery.min.js"></script-->
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendor/owlcarousal/js/owl.carousel.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/script.js"></script>

</body>
</html>
