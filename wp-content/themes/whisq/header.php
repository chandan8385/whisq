<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package whisq
 */

// Get Logo from wordpress customizer
$custom_logo_id = get_theme_mod( 'custom_logo' );
$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	 <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Owl carousal CSS -->
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/vendor/owlcarousal/css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/vendor/owlcarousal/css/owl.theme.green.min.css" rel="stylesheet">

    <!-- Font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="<?php bloginfo('stylesheet_directory');?>/assets/css/full.css" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'whisq' ); ?></a>

	<header>
		<!-- Navigation -->
	    <nav class="navbar navbar-expand-lg navbar-light white">
	      <div class="container">

					

				<!-- Search Modal -->
				<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Search</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">

				      	<form id="mobSearch" role="search" method="get" class="search-form mob_search" action="http://localhost/whisq/" style="display: block;">
							<label>
								<span class="screen-reader-text">Search for:</span>
								<input type="search" class="search-field" placeholder="Search …" value="" name="s">
							</label>
							<input type="submit" class="search-submit" value="Search">
						</form>
				     
				      </div>
				      
				    </div>
				  </div>
				</div>


	      		
	      		
	        
	        <a class="navbar-brand" href="<?php echo get_option("siteurl"); ?>"><img src="<?php echo $image[0]; ?>"/></a>
	        

             <?php
             wp_nav_menu( array(
              'theme_location' => 'menu-1',
              'container'      => 'div',
              'container_class'=> 'collapse navbar-collapse',
              'container_id'   => 'navbarResponsive',
              'menu_class'     => 'navbar-nav',              
             ));
             ?>

				

	        <a href="#searchModal" data-toggle="modal" id="deskSearch"> <i class="fa fa-search"></i></a>
	      </div>
	    </nav>

	</header>