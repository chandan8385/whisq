<?php
/**
 * Template Name: Home Page
 *
 * @package whisq
 */

//Advanced Custom Fields


$first_image = get_field('image_1');
$first_title = get_field('title_1');
$second_image = get_field('image_2');
$second_title = get_field('title_2');
$third_image = get_field('image_3');
$third_title = get_field('title_3');
$fourth_image = get_field('image_4');
$fourth_title = get_field('title_4');
$hover_icon   = get_field('hover_icon');


$reipe_title  = get_field('reipe_title');
$recipe_sub_title  = get_field('recipe_sub_title');
$recipe_1_image  = get_field('recipe_1_image');
$recipe_1_title  = get_field('recipe_1_title');
$recipe_1_decription  = get_field('recipe_1_decription');
$recipe_2_image  = get_field('recipe_2_image');
$recipe_2_title  = get_field('recipe_2_title');
$recipe_2_decription	  = get_field('recipe_2_decription');


$testimonial_1 =get_field('testimonial_1');
$author_1 =get_field('author_1');
$testimonial_2 =get_field('testimonial_2');
$author_2 =get_field('author_2');
$testimonial_3 =get_field('testimonial_3');
$author_3 =get_field('author_3');
$testimonial_background_image = get_field('testimonial_background_image');
$header_icon = get_field('header_icon');

get_header(); ?>


<?php echo do_shortcode( '[rev_slider alias="homepage"]' ); ?>


<section id="four-box" class="inGrid fboxes">
      <div class="container">
          <div class="row fbox">
            <div class="col-md-6 first-box ws-box"> 

                <div class="fb-wrap">
                  <img src="<?php echo $first_image; ?>" alt="Avatar" class="fb-img" style="width:100%">
                  <div class="fb-middle">
                    <div class="fb-icon"><img src="<?php echo $hover_icon; ?>" alt="Avatar"></div>
                  </div>
                </div>
                <div class="card" style="">
                    <div class="card-body f-box-card-body">
                        <h4 class="card-title fb-title"><?php echo $first_title; ?></h4>          
                    </div>
                </div>
                
            </div>
            <div class="col-md-6 second-box ws-box">   
                <div class="fb-wrap">
                  <img src="<?php echo $second_image; ?>" alt="Avatar" class="fb-img" style="width:100%">
                  <div class="fb-middle">
                    <div class="fb-icon"><img src="<?php echo $hover_icon; ?>" alt="Avatar"></div>
                  </div>
                </div>
                <div class="card" style="">
                    <div class="card-body f-box-card-body">
                        <h4 class="card-title fb-title"><?php echo $second_title; ?></h4>          
                    </div>
                </div>
            </div>
            <div class="col-md-6 third-box ws-box">   
                <div class="fb-wrap">
                  <img src="<?php echo $third_image; ?>" alt="Avatar" class="fb-img" style="width:100%">
                  <div class="fb-middle">
                    <div class="fb-icon"><img src="<?php echo $hover_icon; ?>" alt="Avatar"></div>
                  </div>
                </div>
                <div class="card" style="">
                    <div class="card-body f-box-card-body">
                        <h4 class="card-title fb-title"><?php echo $third_title; ?></h4>          
                    </div>
                </div>
            </div>
            <div class="col-md-6 fourth-box ws-box"> 
                <div class="fb-wrap">
                  <img src="<?php echo $fourth_image; ?>" alt="Avatar" class="fb-img" style="width:100%">
                  <div class="fb-middle">
                    <div class="fb-icon"><img src="<?php echo $hover_icon; ?>" alt="Avatar"></div>
                  </div>
                </div>
                <div class="card" style="">
                    <div class="card-body f-box-card-body">
                        <h4 class="card-title fb-title"><?php echo $fourth_title; ?></h4>          
                    </div>
                </div>
            </div>
          </div>
      </div>   
    </section>

    <section id="testimonial" class="inGrid" style="background-image: url('<?php echo $testimonial_background_image; ?>');">
      <div class="container">
          <div class="row testimonial">
            <div class="col-md-7 offset-md-5 testi">
                <img src="<?php echo $header_icon; ?>;">
                <div class="owl-carousel owl-theme">
                  <div class="item">
                    <p><?php echo $testimonial_1; ?></p>
                    <p>- <?php echo $author_1; ?></p>
                  </div>
                  <div class="item">
                    <p><?php echo $testimonial_2; ?></p>
                    <p>- <?php echo $author_2; ?></p>
                  </div>
                  <div class="item">
                    <p><?php echo $testimonial_3; ?></p>
                    <p>- <?php echo $author_3; ?></p>
                  </div>
                </div>
            </div>
          </div>
       </div>
    </section> 
            
    <section id="recipe" class="inGrid">
      <div class="container">
          <div class="row recipe-header">
            <div class="col-md-12">
              <h4 class="header-title"><?php echo $reipe_title; ?></h4>
              <p class="headet-sub-text"><?php echo $recipe_sub_title; ?></p>
            </div>
          </div>    
      </div>

      <div class="container">
          <div class="row recipe-item">
            <div class="col-md-6">
              <div class="card" style="">
                  <img class="card-img-top" src="<?php echo $recipe_1_image ?>" alt="image" style="width:100%">
                  <div class="card-body">
                    <h4 class="card-title"><?php echo $recipe_1_title; ?></h4>
                    <p class="card-text"><?php echo $recipe_1_decription;?></p>
                    
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card" style="">
                  <img class="card-img-top" src="<?php echo $recipe_2_image ?>" alt="image" style="width:100%">
                  <div class="card-body">
                    <h4 class="card-title"><?php echo $recipe_2_title; ?></h4>
                    <p class="card-text"><?php echo $recipe_2_decription;?></p>
                    
                  </div>
              </div>
            </div>
          </div>
      </div>
    </section>



<?php

get_footer();
