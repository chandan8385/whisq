jQuery(function ($) {
  
  $('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    autoplay:true,
	    autoplayTimeout:1500,
	    items:1
	})


	$('ul.navbar-nav > li').addClass('nav-item');
	$('ul.navbar-nav > li > a').addClass('nav-link');

	//add search icon for mobile
	$('.mega-menu-toggle').prepend('<div class="si-wrap"><a href="#searchModal" data-toggle="modal"> <i class="fa fa-search"></i></a></div>');

	$('#myModal').on('shown.bs.modal', function () {
  	$('#myInput').trigger('focus')
	})
   
});